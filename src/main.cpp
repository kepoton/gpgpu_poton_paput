#include <vulkan/vulkan.hpp>
#include <iostream>
#include <fstream>
#include <stdio.h>

struct Buffer {
	VkBuffer buffer;
	VkDeviceMemory bufferMemory;
	VkDevice* device;
	uint32_t  size_t;
};

void writeBufferAsBMP(const float* const data, unsigned int w, unsigned int h, std::string filename);
VkBuffer createBuffer(const VkDevice& device, uint32_t bufferSize, VkBufferUsageFlags usage);
void copyBuffer(Buffer dst, Buffer src, VkDevice device, int queueFamilyIndex);
void sendData(Buffer& gpu_buffer, VkDevice& logicalDevice, VkPhysicalDevice& selectedPhysicalDevice, int selectedQueueFamilyIndex, float* data, uint32_t buffer_size);
void getData(Buffer& gpu_buffer, VkDevice& logicalDevice, VkPhysicalDevice& selectedPhysicalDevice, int selectedQueueFamilyIndex, float* data, uint32_t buffer_size);

// Image dimensions
const unsigned int width	= 1024;
const unsigned int height   = 1024;

// Buffer size according to the image dimensions
const unsigned int BUFFER_SIZE = width * height * 3 * sizeof(float);
const char* const validationLayer = "VK_LAYER_KHRONOS_validation";

void main() {

	//Create Vulkan Instance
	VkApplicationInfo applicationInfo = {};
	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.pApplicationName = "Vulkan lesson";
	applicationInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	applicationInfo.pEngineName = "No Engine";
	applicationInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	applicationInfo.apiVersion = VK_API_VERSION_1_0;

	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &applicationInfo;
	createInfo.enabledExtensionCount = 0;
	createInfo.enabledLayerCount = 1;
	createInfo.ppEnabledLayerNames = &validationLayer;

	VkInstance instance = {};
	VkResult result = vkCreateInstance(&createInfo, nullptr, &instance);

	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error during the Vulkan Instance creation");
	}

	//Store all devices
	uint32_t physicalDevicesCount = 0;
	vkEnumeratePhysicalDevices(instance, &physicalDevicesCount, nullptr);

	std::vector<VkPhysicalDevice> physicalDevices(physicalDevicesCount);
	vkEnumeratePhysicalDevices(instance, &physicalDevicesCount, physicalDevices.data());

	//Selected DISCRETE_GPU device
	VkPhysicalDevice* selectedPhysicalDevice;
	VkPhysicalDeviceProperties physicalDeviceProperties;
	VkPhysicalDeviceFeatures physicalDeviceFeatures;

	for (auto& device : physicalDevices) {
		vkGetPhysicalDeviceProperties(device, &physicalDeviceProperties);
		if (physicalDeviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
			vkGetPhysicalDeviceFeatures(device, &physicalDeviceFeatures);
			selectedPhysicalDevice = &device;
			break;
		}
	}

	//Store all queue families
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(*selectedPhysicalDevice, &queueFamilyCount, nullptr);

	std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(*selectedPhysicalDevice, &queueFamilyCount, queueFamilyProperties.data());


	//Selected COMPUTE_BIT queue family
	VkQueueFamilyProperties* selectedQueueFamily;
	uint32_t selectedQueueFamilyIndex = 0;

	for (auto& queueFamily : queueFamilyProperties) {
		if (queueFamily.queueFlags & VK_QUEUE_COMPUTE_BIT) {
			selectedQueueFamily = &queueFamily;
			break;
		}
		selectedQueueFamilyIndex++;
	}
	
	/*********************************************TP03**************************************************/


	//Configure queue
	VkDeviceQueueCreateInfo deviceQueueCreateInfo = {};
	deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	deviceQueueCreateInfo.queueFamilyIndex = selectedQueueFamilyIndex;
	deviceQueueCreateInfo.queueCount = 1;

	float priority = 1.0f;
	deviceQueueCreateInfo.pQueuePriorities = &priority;

	//Configure logical device
	VkDeviceCreateInfo deviceCreateInfo = {};
	deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCreateInfo.pQueueCreateInfos = &deviceQueueCreateInfo; // !!
	deviceCreateInfo.queueCreateInfoCount = 1; // !!
	deviceCreateInfo.pEnabledFeatures = &physicalDeviceFeatures;
	deviceCreateInfo.enabledExtensionCount = 0;
	deviceCreateInfo.enabledLayerCount = 0;
	

	//Create logical device
	VkDevice logicalDevice;
	result = vkCreateDevice(*selectedPhysicalDevice, &deviceCreateInfo, nullptr, &logicalDevice);

	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error during the logical device creation");
	}

	//Get queue handle from logical device
	VkQueue queue;

	vkGetDeviceQueue(logicalDevice, selectedQueueFamilyIndex, 0, &queue);

	//Own buffer class
	Buffer gpu_buffer;
	gpu_buffer.device = &logicalDevice;
	gpu_buffer.size_t = BUFFER_SIZE;
	gpu_buffer.buffer = createBuffer(logicalDevice, gpu_buffer.size_t, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);


	//Buffer memory managment
	VkMemoryRequirements memoryRequirements;
	vkGetBufferMemoryRequirements(logicalDevice, gpu_buffer.buffer, &memoryRequirements);

	VkPhysicalDeviceMemoryProperties memoryProperties;
	vkGetPhysicalDeviceMemoryProperties(*selectedPhysicalDevice, &memoryProperties);
	uint32_t memoryTypeIndex = 0;

	for (; memoryTypeIndex < 32; memoryTypeIndex++) {
		if (((VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT & memoryProperties.memoryTypes[memoryTypeIndex].propertyFlags) == VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) && (memoryRequirements.memoryTypeBits & (1u << memoryTypeIndex)) ) {
			break;
		}
	}

	VkMemoryAllocateInfo memoryAllocateInfo = {};
	memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memoryAllocateInfo.allocationSize = memoryRequirements.size;
	memoryAllocateInfo.memoryTypeIndex = memoryTypeIndex;


	result = vkAllocateMemory(logicalDevice, &memoryAllocateInfo, nullptr, &(gpu_buffer.bufferMemory));
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error during memory allocation");
	}

	vkBindBufferMemory(logicalDevice, gpu_buffer.buffer, gpu_buffer.bufferMemory, 0);

	/*********************************************TP04**************************************************/

	/*float data[BUFFER_SIZE/sizeof(float)];
	for (int i = 0; i < BUFFER_SIZE/sizeof(float); i++) {
		data[i] = i;
	}

	sendData(gpu_buffer, logicalDevice, *selectedPhysicalDevice, selectedQueueFamilyIndex, data, BUFFER_SIZE);
	
	for (int i = 0; i < BUFFER_SIZE/sizeof(float); i++) {
		data[i] = 0;
	}

	getData(gpu_buffer, logicalDevice, *selectedPhysicalDevice, selectedQueueFamilyIndex, data, BUFFER_SIZE);

	for (int i = 0; i < BUFFER_SIZE/ sizeof(float); i++) {
		std::cout << "value : " << data[i] << std::endl;
	}*/

	/*********************************************TP05**************************************************/

	FILE* file = fopen("shader_mandelbrot.spv", "rb");
	if (!file) {
		throw std::runtime_error("Error during shader file opening");
	}
	
	fseek(file, 0, SEEK_END);
	int size = ftell(file);
	rewind(file);

	std::vector<char> code(size);
	fread(code.data(), size, 1, file);
	fclose(file);


	/*auto shader = std::ifstream("shader.spv", std::ios::binary);
	
	while (shader.good()) {
		code.push_back(shader.get());
	}*/

	VkShaderModuleCreateInfo shaderModuleCreateInfo = {};
	shaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	shaderModuleCreateInfo.codeSize = ((code.size() + 3) / 4) * 4;
	shaderModuleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

	VkShaderModule shaderModule;
	result = vkCreateShaderModule(logicalDevice, &shaderModuleCreateInfo, nullptr, &shaderModule);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error during shader creation");
	}

	/*********************************************TP06**************************************************/

	std::vector< VkDescriptorSetLayoutBinding> descriptorSetLayoutBindings;

	VkDescriptorSetLayoutBinding descriptorSetLayoutBinding = {};
	descriptorSetLayoutBinding.binding = 0;
	descriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	descriptorSetLayoutBinding.descriptorCount = 1;
	descriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
	descriptorSetLayoutBinding.pImmutableSamplers = nullptr;

	descriptorSetLayoutBindings.push_back(descriptorSetLayoutBinding);

	VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = {};
	descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptorSetLayoutCreateInfo.pBindings = &descriptorSetLayoutBinding;
	descriptorSetLayoutCreateInfo.bindingCount = 1;
	
	VkDescriptorSetLayout descriptorSetLayout = {};
	vkCreateDescriptorSetLayout(logicalDevice, &descriptorSetLayoutCreateInfo, nullptr, &descriptorSetLayout);

	VkDescriptorPoolSize descriptorPoolSize = {};
	descriptorPoolSize.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	descriptorPoolSize.descriptorCount = 1;

	VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {};
	descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	descriptorPoolCreateInfo.poolSizeCount = 1;
	descriptorPoolCreateInfo.pPoolSizes = &descriptorPoolSize;
	descriptorPoolCreateInfo.maxSets = 1;

	VkDescriptorPool descriptorPool = {};
	result = vkCreateDescriptorPool(logicalDevice, &descriptorPoolCreateInfo, nullptr, &descriptorPool);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error during descriptor pool creation");
	}

	VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {};
	descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	descriptorSetAllocateInfo.descriptorPool = descriptorPool;
	descriptorSetAllocateInfo.descriptorSetCount = 1;
	descriptorSetAllocateInfo.pSetLayouts = &descriptorSetLayout;

	VkDescriptorSet descriptorSets = {};
	result = vkAllocateDescriptorSets(logicalDevice, &descriptorSetAllocateInfo, &descriptorSets);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error during descriptor set allocation");
	}

	VkDescriptorBufferInfo descriptorBufferInfo = {};
	descriptorBufferInfo.buffer = gpu_buffer.buffer;
	descriptorBufferInfo.offset = 0;
	descriptorBufferInfo.range = gpu_buffer.size_t;

	VkWriteDescriptorSet writeDescriptorSet = {};
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.dstSet = descriptorSets;
	writeDescriptorSet.dstBinding = 0;
	writeDescriptorSet.dstArrayElement = 0;
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	writeDescriptorSet.descriptorCount = 1;
	writeDescriptorSet.pBufferInfo = &descriptorBufferInfo;

	vkUpdateDescriptorSets(logicalDevice, 1, &writeDescriptorSet, 0, nullptr);

	VkPushConstantRange pushConstantRange = {};
	pushConstantRange.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
	pushConstantRange.offset = 0;
	pushConstantRange.size = 2*sizeof(unsigned int);

	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {};
	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCreateInfo.setLayoutCount = 1;
	pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;
	pipelineLayoutCreateInfo.pushConstantRangeCount = 1;
	pipelineLayoutCreateInfo.pPushConstantRanges = &pushConstantRange;


	VkPipelineLayout pipelineLayout = {};
	result = vkCreatePipelineLayout(logicalDevice, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error during pipeline layout creation");
	}

	VkPipelineShaderStageCreateInfo pipelineShaderStageCreateInfo = {};
	pipelineShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	pipelineShaderStageCreateInfo.stage = VK_SHADER_STAGE_COMPUTE_BIT;
	pipelineShaderStageCreateInfo.module = shaderModule;
	pipelineShaderStageCreateInfo.pName = "main";


	VkComputePipelineCreateInfo computePipelineCreateInfo = {};
	computePipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
	computePipelineCreateInfo.stage = pipelineShaderStageCreateInfo;
	computePipelineCreateInfo.layout = pipelineLayout;

	VkPipeline pipeline = {};
	result = vkCreateComputePipelines(logicalDevice, VK_NULL_HANDLE, 1, &computePipelineCreateInfo, nullptr, &pipeline);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error during pipeline creation");
	}

	//Command buffer (copied from copyBuffer function)
	VkCommandPool commandPool;
	VkCommandPoolCreateInfo commandPoolCreateInfo = {};
	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.queueFamilyIndex = selectedQueueFamilyIndex;
	commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;

	result = vkCreateCommandPool(logicalDevice, &commandPoolCreateInfo, nullptr, &commandPool);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error command pool creation");
	}

	VkCommandBufferAllocateInfo commandBufferAllocationInfo = {};
	commandBufferAllocationInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocationInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocationInfo.commandBufferCount = 1; //Pas dans l'�nonc�
	commandBufferAllocationInfo.commandPool = commandPool;

	VkCommandBuffer commandBuffer;
	result = vkAllocateCommandBuffers(logicalDevice, &commandBufferAllocationInfo, &commandBuffer);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error command buffer allocation");
	}

	VkCommandBufferBeginInfo commandBufferBeginInfo = {};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	//End of copied code
	unsigned int bufferSize = gpu_buffer.size_t / (3 * sizeof(float));
	result = vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error command buffer begin");
	}

	/*********************************************TP07**************************************************/


	vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
	vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1, &descriptorSets, 0, nullptr);

	//vkCmdPushConstants( commandBuffer, pipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, uint32_t(sizeof(unsigned int)), &bufferSize);
	//vkCmdDispatch(commandBuffer, bufferSize, 1, 1);
	uint32_t* args = (uint32_t*)malloc(2 * sizeof(uint32_t));
	args[0] = width;
	args[1] = height;
	vkCmdPushConstants(commandBuffer, pipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, uint32_t(2*sizeof(unsigned int)), args);
	vkCmdDispatch(commandBuffer, width, height, 1);

	result = vkEndCommandBuffer(commandBuffer);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error command buffer end");
	}

	VkFenceCreateInfo fenceCreateInfo = {};
	fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

	VkFence fence;
	result = vkCreateFence(logicalDevice, &fenceCreateInfo, nullptr, &fence);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error fence creation");
	}

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;

	result = vkQueueSubmit(queue, 1, &submitInfo, fence);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error fence submit");
	}

	result = vkWaitForFences(logicalDevice, 1, &fence, true, uint64_t(-1));
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error fence waiting");
	}

	float* data = (float*)malloc(gpu_buffer.size_t);
	getData(gpu_buffer, logicalDevice, *selectedPhysicalDevice, selectedQueueFamilyIndex, data, gpu_buffer.size_t);

	writeBufferAsBMP(data, 1024, 1024, "shader_out.bmp");

	free(data);

	/***************************************************************************************************/
	vkDestroyFence(logicalDevice, fence, nullptr);

	vkFreeCommandBuffers(logicalDevice, commandPool, 1, &commandBuffer);
	vkDestroyCommandPool(logicalDevice, commandPool, nullptr);

	vkDestroyPipeline(logicalDevice, pipeline, nullptr);
	vkDestroyPipelineLayout(logicalDevice, pipelineLayout, nullptr);

	vkDestroyDescriptorPool(logicalDevice, descriptorPool, nullptr);
	vkDestroyDescriptorSetLayout(logicalDevice, descriptorSetLayout, nullptr);

	vkDestroyShaderModule(logicalDevice, shaderModule, nullptr);

	vkFreeMemory(logicalDevice, gpu_buffer.bufferMemory, nullptr);
	vkDestroyBuffer(*gpu_buffer.device, gpu_buffer.buffer, nullptr);

	vkDestroyDevice(logicalDevice, nullptr);
	vkDestroyInstance(instance, nullptr);
}

VkBuffer createBuffer(const VkDevice& device, uint32_t bufferSize, VkBufferUsageFlags usage) {
	VkBuffer buffer;
	VkBufferCreateInfo bufferCreateInfo = {};
	bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferCreateInfo.size = bufferSize;
	bufferCreateInfo.usage = usage;
	bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VkResult result = vkCreateBuffer(device, &bufferCreateInfo, nullptr, &buffer);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error during the buffer creation");
	}

	return buffer;
}

void copyBuffer(Buffer dst, Buffer src, VkDevice device, int queueFamilyIndex) {
	//Command pool creation
	VkCommandPool commandPool;
	VkCommandPoolCreateInfo commandPoolCreateInfo = {};
	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.queueFamilyIndex = queueFamilyIndex;
	commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;

	VkResult result = vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPool);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error command pool creation");
	}

	//Command buffer allocation
	VkCommandBufferAllocateInfo commandBufferAllocationInfo = {};
	commandBufferAllocationInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocationInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocationInfo.commandBufferCount = 1; //Pas dans l'�nonc�
	commandBufferAllocationInfo.commandPool = commandPool;

	VkCommandBuffer commandBuffer;
	result = vkAllocateCommandBuffers(device, &commandBufferAllocationInfo, &commandBuffer);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error command buffer allocation");
	}

	VkCommandBufferBeginInfo commandBufferBeginInfo = {};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;


	result = vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error begin command buffer");
	}

	VkBufferCopy bufferCopy = {};
	bufferCopy.srcOffset = 0;
	bufferCopy.dstOffset = 0;
	bufferCopy.size = src.size_t;
	vkCmdCopyBuffer(commandBuffer, src.buffer, dst.buffer, 1, &bufferCopy);

	dst.size_t = src.size_t;

	result= vkEndCommandBuffer(commandBuffer);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error end command buffer");
	}

	//Submit command buffer
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;


	VkQueue queue;
	vkGetDeviceQueue(device, queueFamilyIndex, 0, &queue);

	vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(queue);
	vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);

	
	vkDestroyCommandPool(device, commandPool, nullptr);
}

void sendData(Buffer& gpu_buffer, VkDevice& logicalDevice, VkPhysicalDevice& selectedPhysicalDevice, int selectedQueueFamilyIndex, float* data, uint32_t buffer_size) {
	Buffer stageBuffer;
	stageBuffer.device = &logicalDevice;
	stageBuffer.size_t = buffer_size;
	stageBuffer.buffer = createBuffer(logicalDevice, stageBuffer.size_t, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);

	//Transfer Buffer memory managment
	VkMemoryRequirements stageMemoryRequirements;
	vkGetBufferMemoryRequirements(logicalDevice, stageBuffer.buffer, &stageMemoryRequirements);

	VkPhysicalDeviceMemoryProperties stageMemoryProperties;
	vkGetPhysicalDeviceMemoryProperties(selectedPhysicalDevice, &stageMemoryProperties);
	uint32_t stageMemoryTypeIndex = 0;

	for (; stageMemoryTypeIndex < 32; stageMemoryTypeIndex++) {
		if (((VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT & stageMemoryProperties.memoryTypes[stageMemoryTypeIndex].propertyFlags) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) && (stageMemoryRequirements.memoryTypeBits & (1u << stageMemoryTypeIndex))) {
			break;
		}
	}

	VkMemoryAllocateInfo stageMemoryAllocateInfo = {};
	stageMemoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	stageMemoryAllocateInfo.allocationSize = stageMemoryRequirements.size;
	stageMemoryAllocateInfo.memoryTypeIndex = stageMemoryTypeIndex;


	VkResult result = vkAllocateMemory(logicalDevice, &stageMemoryAllocateInfo, nullptr, &(stageBuffer.bufferMemory));
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error during memory allocation");
	}

	vkBindBufferMemory(logicalDevice, stageBuffer.buffer, stageBuffer.bufferMemory, 0);

	void* driverData;
	vkMapMemory(logicalDevice, stageBuffer.bufferMemory, 0, stageBuffer.size_t, 0, &driverData);
	memcpy(driverData, data, buffer_size);
	vkUnmapMemory(logicalDevice, stageBuffer.bufferMemory);

	copyBuffer(gpu_buffer, stageBuffer, logicalDevice, selectedQueueFamilyIndex);

	vkFreeMemory(logicalDevice, stageBuffer.bufferMemory, nullptr);
	vkDestroyBuffer(*stageBuffer.device, stageBuffer.buffer, nullptr);
}

void getData(Buffer& gpu_buffer, VkDevice& logicalDevice, VkPhysicalDevice& selectedPhysicalDevice, int selectedQueueFamilyIndex, float* data, uint32_t buffer_size) {
	Buffer stageBuffer;
	stageBuffer.device = &logicalDevice;
	stageBuffer.size_t = buffer_size;
	stageBuffer.buffer = createBuffer(logicalDevice, stageBuffer.size_t, VK_BUFFER_USAGE_TRANSFER_DST_BIT);

	//Transfer Buffer memory managment
	VkMemoryRequirements stageMemoryRequirements;
	vkGetBufferMemoryRequirements(logicalDevice, stageBuffer.buffer, &stageMemoryRequirements);

	VkPhysicalDeviceMemoryProperties stageMemoryProperties;
	vkGetPhysicalDeviceMemoryProperties(selectedPhysicalDevice, &stageMemoryProperties);
	uint32_t stageMemoryTypeIndex = 0;

	for (; stageMemoryTypeIndex < 32; stageMemoryTypeIndex++) {
		if (((VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT & stageMemoryProperties.memoryTypes[stageMemoryTypeIndex].propertyFlags) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) && (stageMemoryRequirements.memoryTypeBits & (1u << stageMemoryTypeIndex))) {
			break;
		}
	}

	VkMemoryAllocateInfo stageMemoryAllocateInfo = {};
	stageMemoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	stageMemoryAllocateInfo.allocationSize = stageMemoryRequirements.size;
	stageMemoryAllocateInfo.memoryTypeIndex = stageMemoryTypeIndex;


	VkResult result = vkAllocateMemory(logicalDevice, &stageMemoryAllocateInfo, nullptr, &(stageBuffer.bufferMemory));
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Error during memory allocation");
	}

	vkBindBufferMemory(logicalDevice, stageBuffer.buffer, stageBuffer.bufferMemory, 0);

	copyBuffer(stageBuffer, gpu_buffer, logicalDevice, selectedQueueFamilyIndex);

	void* driverData;
	vkMapMemory(logicalDevice, stageBuffer.bufferMemory, 0, stageBuffer.size_t, 0, &driverData);
	memcpy(data, driverData, buffer_size);
	vkUnmapMemory(logicalDevice, stageBuffer.bufferMemory);

	vkFreeMemory(logicalDevice, stageBuffer.bufferMemory, nullptr);
	vkDestroyBuffer(*stageBuffer.device, stageBuffer.buffer, nullptr);
}

void writeBufferAsBMP(const float* const data, unsigned int w, unsigned int h, std::string filename) {

	unsigned char header[54] = {
		'B','M',0,0,0,0,0,0,0,0,54,0,0,0,40,0,0,0,0,0,0,0,0,0,0,0,1,0,
		24,0,0,0,0,0,0,0,0,0,0x13,0x0B,0,0,0x13,0x0B,0,0,0,0,0,0,0,0,0,0 };

	const unsigned int padSize = (4 - 3 * w % 4) % 4;
	const unsigned int size = w * h * 3 + h * padSize;

	auto fillHeader = [&](unsigned int value, unsigned int index) {
		header[index] = (unsigned char)(value);
		header[index + 1u] = (unsigned char)(value >> 8u);
		header[index + 2u] = (unsigned char)(value >> 16u);
		header[index + 3u] = (unsigned char)(value >> 24u);
	};

	fillHeader(size + 54, 2);
	fillHeader(w, 18);
	fillHeader(h, 22);
	fillHeader(size, 34);

	std::ofstream imageout(filename, std::ios::out | std::ios::binary);
	imageout.write((char*)header, sizeof(header));

	const char pad[3] = { 0,0,0 };
	for (unsigned int y = 0; y < h; ++y) {
		for (unsigned int x = 0; x < w; ++x) {
			unsigned char pixelc[3]{
				unsigned char(255.99f*data[x * 3 + w * y * 3 + 2]),
				unsigned char(255.99f*data[x * 3 + w * y * 3 + 1]),
				unsigned char(255.99f*data[x * 3 + w * y * 3 + 0]),
			};
			imageout.write((char*)pixelc, 3);
		}
		imageout.write(pad, padSize);
	}
}